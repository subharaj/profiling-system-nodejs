const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql2');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('./config/config');
const session = require('express-session');

const cors = require('cors');
// const { SELECT } = require('sequelize/types/query-types');

const port = process.env.PORT || 3000;


app.use(cors());
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(cors({
  origin: 'http://localhost:3000',
  credentials: true
}));

app.use(session({
  secret: 'mySecret',
  resave: false,
  saveUninitialized: false,
}))
// parse requests of content-type - application/json
app.use(bodyParser.json())
// parse requests of content-type - application/x-www-form-urlencoded


const connection = mysql.createConnection({
  host: 'localhost', // MySQL server host
  user: 'root',      // MySQL username
  password: 'password',  // MySQL password
  database: 'profiling_system' // MySQL database name
});

connection.connect(error => {
  if (error) {
    console.error('Error connecting to MySQL: ', error);
  } else {
    console.log('Connected to MySQL database!');
  }
});


// ===================================

// POST route to create a new user
app.post('/api/signup', (req, res) => {
  const { firstname, lastname, phone, email, password, confirmPassword, role } = req.body;

  // Set default role to 'user' if no role is provided
  const defaultRole = 'user';
  const userRole = role || defaultRole;

  // Hash the password
  const hashedPassword = bcrypt.hashSync(password, 10);

  const query = 'INSERT INTO tbl_users (firstname, lastname, phone, email, password, confirmPassword, role) VALUES (?, ?, ?, ?, ?, ?, ?)';
  connection.query(query, [firstname, lastname, phone, email, hashedPassword, confirmPassword, userRole], (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).send('Internal Server Error');
    } else {
      res.status(201).json({ message: "Data created successful" }); // 201: Created
    }
  });
});


app.post('/api/signin', (req, res) => {
  const { email, password } = req.body;

  connection.query('SELECT * FROM tbl_users WHERE email = ?', [email], (error, results) => {
    if (error) {
      console.error('Error retrieving user:', error);
      return res.status(500).json({ error: 'Internal server error' });
    }

    if (results.length === 0) {
      return res.status(401).json({ error: 'No Data Found' });
    }

    const user = results[0];
    const isMatch = bcrypt.compareSync(password, user.password);
    console.log({ isMatch });
    if (!isMatch) {
      return res.status(401).json({ error: 'Invalid email or password' });
    }

    const access = jwt.sign({ email: user.email }, config.jwtSecret, { expiresIn: '1h' });
    const refresh = jwt.sign({ email: user.email }, config.jwtRefreshSecret, { expiresIn: '7d' });

    // Verify access token for expiration before sending the response
    try {
      const decodedToken = jwt.verify(access, config.jwtSecret);
      if (decodedToken.exp < Date.now() / 1000) {
        return res.status(401).json({ error: 'Access token has expired' });
      }
    } catch (verifyError) {
      console.error('Error verifying access token:', verifyError);
      return res.status(500).json({ error: 'Error verifying access token' });
    }

    res.json({
      access,
      refresh,
      firstname: user.firstname,
      email: user.email,
      role: user.role
    });
  });
});

// =====================================


// POST ROUTE TO POST A STATE
app.post('/api/state', (req, res) => {

  const { state_name } = req.body;

  const query = 'INSERT INTO tbl_state (state_name) VALUES (?)';

  connection.query(query, [state_name], (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'State data posted successful..' })
      console.log("State Data Posted Successful");
    }
  })
})

app.get('/api/state', (req, res) => {
  const query = 'SELECT * FROM tbl_state';

  connection.query(query, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});


app.delete('/api/state/:id', (req, res) => {
  const stateId = req.params.id;

  const query = 'DELETE FROM tbl_state WHERE id = ?';
  const values = [stateId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'State not found' });
      } else {
        res.json({ message: 'State deleted successfully' });
      }
    }
  });
});

app.put('/api/state/:id', (req, res) => {
  const stateId = req.params.id;
  const newStateName = req.body.state_name;

  const query = 'UPDATE tbl_state SET state_name = ? WHERE id = ?';
  const values = [newStateName, stateId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'State not found' });
      } else {
        res.json({ message: 'State updated successfully' });
      }
    }
  });
});


// DISTRICT DISTRICT DISTRICT DISTRICT DISTRICT 

app.get('/api/district', (req, res) => {
  const query = "SELECT * FROM tbl_district";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results)
    }
  })
})

app.post('/api/district', (req, res) => {

  const { district_name } = req.body;

  const query = 'INSERT INTO tbl_district (district_name) VALUES (?)';

  connection.query(query, [district_name], (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("District Data Posted Successful");
    }
  })
})

app.delete('/api/district/:id', (req, res) => {
  const districtId = req.params.id;

  const query = 'DELETE FROM tbl_district WHERE id = ?';
  const values = [districtId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'District Deleted Successfully' });
      }
    }
  });
});

app.put('/api/district/:id', (req, res) => {
  const districtId = req.params.id;
  const newDistrictName = req.body.district_name;

  const query = 'UPDATE tbl_district SET district_name = ? WHERE id = ?';
  const values = [newDistrictName, districtId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'District Updated Successfully' });
      }
    }
  });
});


// Municipality Municipality Municipality Municipality Municipality 

app.get('/api/municipality', (req, res) => {
  const query = "SELECT * FROM tbl_municipality";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results)
    }
  })
})

app.post('/api/municipality', (req, res) => {

  const { munici_name, munici_type } = req.body;

  const query = 'INSERT INTO tbl_municipality (munici_name, munici_type) VALUES (?,?)';

  connection.query(query, [munici_name, munici_type], (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("Municipality Data Posted Successful");
    }
  })
})

app.delete('/api/municipality/:id', (req, res) => {
  const municiId = req.params.id;

  const query = 'DELETE FROM tbl_municipality WHERE id = ?';
  const values = [municiId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Municipality Deleted Successfully' });
      }
    }
  });
});

app.put('/api/municipality/:id', (req, res) => {
  const municiId = req.params.id;
  const newMuniciName = req.body.munici_name;
  const newMuniciType = req.body.munici_type;

  const query = 'UPDATE tbl_municipality SET munici_name = ?, munici_type = ? WHERE id = ?';
  const values = [newMuniciName, newMuniciType, municiId];  // Corrected order of values

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Municipality Name Updated Successfully' });
      }
    }
  });
});




// VDC VDC VDC VDC VDC 

app.get('/api/vdc', (req, res) => {
  const query = "SELECT * FROM tbl_vdc";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results)
    }
  })
})

app.post('/api/vdc', (req, res) => {

  const { vdc_name } = req.body;

  const query = 'INSERT INTO tbl_vdc (vdc_name) VALUES (?)';

  connection.query(query, [vdc_name], (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("VDC Data Posted Successful");
    }
  })
})

app.delete('/api/vdc/:id', (req, res) => {
  const vdcId = req.params.id;

  const query = 'DELETE FROM tbl_vdc WHERE id = ?';
  const values = [vdcId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'VDC Deleted Successfully' });
      }
    }
  });
});

app.put('/api/vdc/:id', (req, res) => {
  const vdcId = req.params.id;
  const vdcName = req.body.vdc_name;

  const query = 'UPDATE tbl_vdc SET vdc_name = ? WHERE id = ?';
  const values = [vdcName, vdcId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'VDC Name Updated Successfully' });
      }
    }
  });
});


// Ward Ward Ward Ward Ward 

app.get('/api/ward', (req, res) => {
  const query = "SELECT * FROM tbl_ward";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results)
    }
  })
})

app.post('/api/ward', (req, res) => {

  const { ward_number } = req.body;

  const query = 'INSERT INTO tbl_ward (ward_number) VALUES (?)';

  connection.query(query, [ward_number], (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("ward Data Posted Successful");
    }
  })
})

app.delete('/api/ward/:id', (req, res) => {
  const wardId = req.params.id;

  const query = 'DELETE FROM tbl_ward WHERE ward_id = ?';
  const values = [wardId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'WARD Deleted Successfully' });
      }
    }
  });
});

app.put('/api/ward/:id', (req, res) => {
  const wardId = req.params.id;
  const wardNumber = req.body.ward_number;

  const query = 'UPDATE tbl_ward SET ward_number = ? WHERE ward_id = ?';
  const values = [wardNumber, wardId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Ward Number Updated Successfully' });
      }
    }
  });
});





// संघ संस्था

// app.post('/api/organization', (req, res) => {
//   const {org_name,est_year,org_type,samuha_male_no,samuha_female_no,samuha_other_no,mgmt_male_no,mgmt_female_no,mgmt_other_no,address,ward_id,
//   } = req.body;

//   const wardQuery = 'SELECT ward_id FROM tbl_ward WHERE ward_number = ?';
//   connection.query(wardQuery, [ward_id], (wardError, wardResults) => {
//     if (wardError) {
//       console.error('Error executing ward MySQL query: ', wardError);
//       res.status(500).json({ message: 'Internal Server Error' });
//       return;
//     }

//     if (wardResults.length === 0) {
//       res.status(400).json({ message: 'Ward not found.' });
//       return;
//     }

//     const ward_id = wardResults[0].ward_id; 

//     const orgQuery =
//       'INSERT INTO tbl_organization (org_name, ward_id, est_year, org_type, samuha_male_no, samuha_female_no, samuha_other_no, mgmt_male_no, mgmt_female_no,mgmt_other_no, address) VALUES (?,?,?,?,?,?,?,?,?,?,?)';

//     const orgValues = [org_name,ward_id,est_year,org_type,samuha_male_no,samuha_female_no,samuha_other_no,mgmt_male_no,mgmt_female_no,mgmt_other_no,address,
//     ];

//     connection.query(orgQuery, orgValues, (orgError) => {
//       if (orgError) {
//         console.error('Error executing organization MySQL query: ', orgError);
//         res.status(500).json({ message: 'Internal Server Error' });
//         return;
//       }

//       res.status(200).json({ message: 'Organization Data Posted Successfully.' });
//       console.log('Organization Data Posted Successfully');
//     });
//   });
// });


app.post('/api/organization', (req, res) => {
  const { org_name, est_year, org_type, samuha_male_no, samuha_female_no, samuha_other_no, mgmt_male_no, mgmt_female_no, mgmt_other_no, address, ward_id,
  } = req.body;

  const orgQuery =
    'INSERT INTO tbl_organization (org_name, ward_id, est_year, org_type, samuha_male_no, samuha_female_no, samuha_other_no, mgmt_male_no, mgmt_female_no,mgmt_other_no, address) VALUES (?,?,?,?,?,?,?,?,?,?,?)';

  const orgValues = [org_name, ward_id, est_year, org_type, samuha_male_no, samuha_female_no, samuha_other_no, mgmt_male_no, mgmt_female_no, mgmt_other_no, address,
  ];

  connection.query(orgQuery, orgValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("Organizatoin data posted successful");
    }
  })
})

app.get('/api/organization', (req, res) => {
  const query = "SELECT * FROM tbl_organization";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.delete('/api/organization/:id', (req, res) => { // Fixed parameter order: (req, res)
  const orgId = req.params.id;

  const query = "DELETE FROM tbl_organization WHERE id = ?";
  const values = [orgId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Organization Deleted Successfully" });
      }
    }
  });
});

app.put('/api/organization/:id', (req, res) => {
  const orgId = req.params.id;
  const orgName = req.body.org_name;
  const wardId = req.body.ward_id;
  const estYear = req.body.est_year;
  const orgType = req.body.org_type;
  const samuhaMaleNo = req.body.samuha_male_no;
  const samuhaFmaleNo = req.body.samuha_female_no
  const samuhaOtherNo = req.body.samuha_other_no;
  const mgmtMaleNo = req.body.mgmt_male_no;
  const mgmtFemaleNo = req.body.mgmt_female_no
  const mgmtOtherNo = req.body.mgmt_other_no;
  const address = req.body.address

  const query = 'UPDATE tbl_organization SET org_name=?, ward_id=?, est_year=?, org_type=?, samuha_male_no=?, samuha_female_no=?, samuha_other_no=?, mgmt_male_no=?, mgmt_female_no=?, mgmt_other_no=?, address=? WHERE id=?';
  const values = [orgName, wardId, estYear, orgType, samuhaMaleNo, samuhaFmaleNo, samuhaOtherNo, mgmtMaleNo, mgmtFemaleNo, mgmtOtherNo, address, orgId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Organization Updated Successfully' });
      }
    }
  });
});

// QUERY OF INFRASTRUCTURE

app.post('/api/infrastructure', (req, res) => {
  const { infra_name, ward_id, infra_type, no_of_benefitted, road_length, benefitted_wards, bridge_type, road_type, capacity } = req.body;

  const orgQuery = 'INSERT INTO tbl_infrastructure (infra_name, ward_id, infra_type, no_of_benefitted, road_length, benefitted_wards, bridge_type, road_type, capacity) VALUES (?,?,?,?,?,?,?,?,?)';

  const orgValues = [infra_name, ward_id, infra_type, no_of_benefitted, road_length, benefitted_wards, bridge_type, road_type, capacity];

  connection.query(orgQuery, orgValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("Organizatoin data posted successful");
    }
  })
})

app.get('/api/infrastructure', (req, res) => {
  const query = "SELECT * FROM tbl_infrastructure";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.delete('/api/infrastructure/:id', (req, res) => { // Fixed parameter order: (req, res)
  const infraId = req.params.id;

  const query = "DELETE FROM tbl_infrastructure WHERE id = ?";
  const values = [infraId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Infrastructure Deleted Successfully" });
      }
    }
  });
});

app.put('/api/infrastructure/:id', (req, res) => {
  const infraId = req.params.id;
  const infraName = req.body.infra_name;
  const wardId = req.body.ward_id;
  const infraType = req.body.infra_type;
  const noOfBenefitted = req.body.no_of_benefitted;
  const roadLength = req.body.road_length;
  const benefittedWards = req.body.benefitted_wards;
  const bridgeType = req.body.bridge_type;
  const roadType = req.body.road_type;
  const capacitys = req.body.capacity;

  const query = 'UPDATE tbl_infrastructure SET infra_name=?, ward_id=?, infra_type=?, no_of_benefitted=?, road_length=?, benefitted_wards=?, bridge_type=?, road_type=?, capacity=? WHERE id=?';
  const values = [infraName, wardId, infraType, noOfBenefitted, roadLength, benefittedWards, bridgeType, roadType, capacitys, infraId];


  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Infrastructure Updated Successfully' });
      }
    }
  });
});

// QUERY OF DRINKIING

app.post('/api/drinking', (req, res) => {
  const { name, ward_id, beneficiary_wards, households_benefited_no, builtBy, current_situation, builtYear } = req.body;

  const drinkQuery = 'INSERT INTO tbl_drinking (name, ward_id, beneficiary_wards, households_benefited_no, builtBy, current_situation, builtYear) VALUES (?,?,?,?,?,?,?)';

  const drinkValues = [name, ward_id, beneficiary_wards, households_benefited_no, builtBy, current_situation, builtYear];

  connection.query(drinkQuery, drinkValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("Drinking data posted successful");
    }
  })
})

app.get('/api/drinking', (req, res) => {
  const query = "SELECT * FROM tbl_drinking";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/drinking/:id', (req, res) => {
  const id = req.params.id;
  const { name, ward_id, beneficiary_wards, households_benefited_no, builtBy, current_situation, builtYear } = req.body;

  const query = 'UPDATE tbl_drinking SET name=?, ward_id=?, beneficiary_wards=?, households_benefited_no=?, builtBy=?, current_situation=?, builtYear=? WHERE id=?';
  const values = [name, ward_id, beneficiary_wards, households_benefited_no, builtBy, current_situation, builtYear, id];


  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Drinking Updated Successfully' });
      }
    }
  });
});

app.delete('/api/drinking/:id', (req, res) => { // Fixed parameter order: (req, res)
  const infraId = req.params.id;

  const query = "DELETE FROM tbl_drinking WHERE id = ?";
  const values = [infraId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});

// QUERY OF IRRIGATION

app.post('/api/irrigation', (req, res) => {
  const { irri_name, irri_type, irri_period, households_benefited_no, builtBy, builtYear, irri_area, current_situation, beneficiary_wards } = req.body;

  const drinkQuery = 'INSERT INTO tbl_irrigation (irri_name, irri_type, irri_period, households_benefited_no, builtBy, builtYear, irri_area, current_situation, beneficiary_wards) VALUES (?,?,?,?,?,?,?,?,?)';

  const drinkValues = [irri_name, irri_type, irri_period, households_benefited_no, builtBy, builtYear, irri_area, current_situation, beneficiary_wards];

  connection.query(drinkQuery, drinkValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Data Posted Successful..' })
      console.log("Irrigation data posted successful");
    }
  })
})

app.get('/api/irrigation', (req, res) => {
  const query = "SELECT * FROM tbl_irrigation";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/irrigation/:id', (req, res) => {
  const id = req.params.id;
  const { irri_name, irri_type, irri_period, households_benefited_no, builtBy, builtYear, irri_area, current_situation, beneficiary_wards } = req.body;

  const query = 'UPDATE tbl_irrigation SET irri_name=?, irri_type=?, irri_period=?, households_benefited_no=?, builtBy=?, builtYear=?, irri_area=?, current_situation=?, beneficiary_wards=? WHERE id=?';
  const values = [irri_name, irri_type, irri_period, households_benefited_no, builtBy, builtYear, irri_area, current_situation, beneficiary_wards, id];


  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Irrigation Updated Successfully' });
      }
    }
  });
});

app.delete('/api/irrigation/:id', (req, res) => { // Fixed parameter order: (req, res)
  const irriId = req.params.id;

  const query = "DELETE FROM tbl_irrigation WHERE id = ?";
  const values = [irriId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});


// QUERY OF FOREST

app.post('/api/forest', (req, res) => {
  const { forest_name, ward_id, forest_type, forest_res, area, forest_graze, forest_theft, forest_households, main_tree, main_grass, main_herb, main_medicine } = req.body;

  const forestQuery = 'INSERT INTO tbl_forest (forest_name,ward_id, forest_type, forest_res, area, forest_graze, forest_theft, forest_households, main_tree, main_grass, main_herb, main_medicine) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';

  const forestValues = [forest_name, ward_id, forest_type, forest_res, area, forest_graze, forest_theft, forest_households, main_tree, main_grass, main_herb, main_medicine];

  connection.query(forestQuery, forestValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Forest Data Posted Successful..' })
      console.log("Forest data posted successful");
    }
  })
})

app.get('/api/forest', (req, res) => {
  const query = "SELECT * FROM tbl_forest";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/forest/:id', (req, res) => {
  const forestId = req.params.id;
  const {
    forest_name,
    ward_id,
    forest_type,
    forest_res,
    area,
    forest_graze,
    forest_theft,
    forest_households,
    main_tree,
    main_grass,
    main_herb,
    main_medicine } = req.body;

  const query = 'UPDATE tbl_forest SET forest_name=?, ward_id=?, forest_type=?, forest_res=?, area=?, forest_graze=?, forest_theft=?, forest_households=?, main_tree=?, main_grass=?, main_herb=?, main_medicine=? WHERE forest_id=?';
  const values = [forest_name, ward_id, forest_type, forest_res, area, forest_graze, forest_theft, forest_households, main_tree, main_grass, main_herb, main_medicine, forestId];


  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'forest Updated Successfully' });
      }
    }
  });
});

app.delete('/api/forest/:id', (req, res) => { // Fixed parameter order: (req, res)
  const forestId = req.params.id;

  const query = "DELETE FROM tbl_forest WHERE forest_id = ?";
  const values = [forestId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});


// QUERY OF ENVIRONMENT DISASTER

app.post('/api/disaster', (req, res) => {
  const { disaster_name, disaster_type, disaster_reason, disaster_area, disaster_effect, disaster_prevention, disasterWard, disasterYear } = req.body;

  const disasterQuery = 'INSERT INTO tbl_environment_disaster (disaster_name, disaster_type, disaster_reason, disaster_area, disaster_effect, disaster_prevention, disasterWard, disasterYear) VALUES (?,?,?,?,?,?,?,?)';

  const disasterValues = [disaster_name, disaster_type, disaster_reason, disaster_area, disaster_effect, disaster_prevention, disasterWard, disasterYear];

  connection.query(disasterQuery, disasterValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Disaster Data Posted Successful..' })
      console.log("Disaster data posted successful..");
    }
  })
})

app.get('/api/disaster', (req, res) => {
  const query = "SELECT * FROM tbl_environment_disaster";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/disaster/:id', (req, res) => {
  const disasterId = req.params.id;
  const { disaster_name, disaster_type, disaster_reason, disaster_area, disaster_effect, disaster_prevention, disasterWard, disasterYear } = req.body;

  const query = 'UPDATE tbl_environment_disaster SET disaster_name=?, disaster_type=?, disaster_reason=?, disaster_area=?, disaster_effect=?, disaster_prevention=?, disasterWard=?, disasterYear=? WHERE id=?';
  const values = [disaster_name, disaster_type, disaster_reason, disaster_area, disaster_effect, disaster_prevention, disasterWard, disasterYear, disasterId];


  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Disaster Data Updated Successfully' });
      }
    }
  });
});

app.delete('/api/disaster/:id', (req, res) => { // Fixed parameter order: (req, res)
  const disasterId = req.params.id;

  const query = "DELETE FROM tbl_environment_disaster WHERE id = ?";
  const values = [disasterId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});

// QUERY OF INCOME STATEMENT

app.post('/api/income-statement', (req, res) => {
  const { income_title, rate, mood } = req.body;

  const incomeQuery = 'INSERT INTO tbl_income_statement (income_title, rate, mood) VALUES (?,?,?)';

  const incomeValues = [income_title, rate, mood];

  connection.query(incomeQuery, incomeValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Income Statement Data Posted Successful..' })
      console.log("Income Statement data posted successful..");
    }
  })
})

app.get('/api/income-statement', (req, res) => {
  const query = "SELECT * FROM tbl_income_statement";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/income-statement/:id', (req, res) => {
  const incomeId = req.params.id;
  const { income_title, rate, mood } = req.body;

  const query = 'UPDATE tbl_income_statement SET income_title = ?, rate = ?, mood = ? WHERE id=?';
  const values = [income_title, rate, mood, incomeId];


  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Income Statement Data Updated Successfully' });
      }
    }
  });
});

app.delete('/api/income-statement/:id', (req, res) => { // Fixed parameter order: (req, res)
  const incomeId = req.params.id;

  const query = "DELETE FROM tbl_income_statement WHERE id = ?";
  const values = [incomeId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});


// QUERY OF SCHOOL

app.post('/api/school', (req, res) => {
  const { schoolname, ward_id, est_date, schooltype, schoollvl, temp_building_no, perm_building_no, temp_class_no, perm_class_no, toilet_type, toilet_no, drinking_services, admit_rate, school_leaving_rate, mgmt_male, mgmt_female, mgmt_other } = req.body;

  const schoolQuery = 'INSERT INTO tbl_school (schoolname, ward_id, est_date, schooltype, schoollvl, temp_building_no, perm_building_no, temp_class_no, perm_class_no, toilet_type, toilet_no, drinking_services, admit_rate, school_leaving_rate, mgmt_male, mgmt_female, mgmt_other) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

  // Convert the 'est_date' string to an integer
const estDateAsInteger = parseInt(est_date, 10); 

  const schoolValues = [schoolname, ward_id, estDateAsInteger, schooltype, schoollvl, temp_building_no, perm_building_no, temp_class_no, perm_class_no, toilet_type, toilet_no, drinking_services, admit_rate, school_leaving_rate, mgmt_male, mgmt_female, mgmt_other];

  connection.query(schoolQuery, schoolValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'School Data Posted Successful..' })
      console.log("School data posted successful..");
    }
  })
})

app.get('/api/school', (req, res) => {
  const query = "SELECT * FROM tbl_school";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/school/:id', (req, res) => {
  const schoolId = req.params.id;
  const { schoolname, ward_id, est_date, schooltype, schoollvl, temp_building_no, perm_building_no, temp_class_no, perm_class_no, toilet_type, toilet_no, drinking_services, admit_rate, school_leaving_rate, mgmt_male, mgmt_female, mgmt_other } = req.body;

  const query = 'UPDATE tbl_school SET schoolname=?, ward_id=?, est_date=?, schooltype=?, schoollvl=?, temp_building_no=?, perm_building_no=?, temp_class_no=?, perm_class_no=?, toilet_type=?, toilet_no=?, drinking_services=?, admit_rate=?, school_leaving_rate=?, mgmt_male=?, mgmt_female=?, mgmt_other=? WHERE id=?';
  const values = [schoolname, ward_id, est_date, schooltype, schoollvl, temp_building_no, perm_building_no, temp_class_no, perm_class_no, toilet_type, toilet_no, drinking_services, admit_rate, school_leaving_rate, mgmt_male, mgmt_female, mgmt_other, schoolId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'School Data Updated Successfully' });
      }
    }
  });
});


app.delete('/api/school/:id', (req, res) => { // Fixed parameter order: (req, res)
  const schoolId = req.params.id;

  const query = "DELETE FROM tbl_school WHERE id = ?";
  const values = [schoolId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});


// QUERY OF SCHOOL

app.post('/api/industry', (req, res) => {
  const { industryname, industryward_id, industry_type, industry_currentstatus,industry_product, industry_income, emp_male, emp_female, emp_other } = req.body;

  const schoolQuery = 'INSERT INTO tbl_industry (industryname, industryward_id, industry_type, industry_currentstatus, industry_product, industry_income, emp_male, emp_female, emp_other) VALUES (?,?,?,?,?,?,?,?,?)';

  const schoolValues = [industryname, industryward_id, industry_type, industry_currentstatus,industry_product, industry_income, emp_male, emp_female, emp_other];

  connection.query(schoolQuery, schoolValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Industry Posted Successful..' })
      console.log("Industry posted successful..");
    }
  })
})

app.get('/api/industry', (req, res) => {
  const query = "SELECT * FROM tbl_industry";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/industry/:id', (req, res) => {
  const industryId = req.params.id;
  const { industryname, industryward_id, industry_type, industry_currentstatus,industry_product, industry_income, emp_male, emp_female, emp_other } = req.body;

  const query = 'UPDATE tbl_industry SET industryname=?, industryward_id=?, industry_type=?, industry_currentstatus=?, industry_product=?, industry_income=?, emp_male=?, emp_female=?, emp_other=? WHERE id=?';
  const values = [industryname, industryward_id, industry_type, industry_currentstatus,industry_product, industry_income, emp_male, emp_female, emp_other, industryId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Industry Updated Successfully' });
      }
    }
  });
});


app.delete('/api/industry/:id', (req, res) => { // Fixed parameter order: (req, res)
  const industryId = req.params.id;

  const query = "DELETE FROM tbl_industry WHERE id = ?";
  const values = [industryId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});


// QUERY OF POLITICS

app.post('/api/politics', (req, res) => {
  const { politicsname, comm_male, comm_female, comm_other, reli_male, reli_female, reli_other, dalit_male, dalit_female, dalit_other, tribes_male, tribes_female, tribes_other, other_male, other_female, other_other } = req.body;

  const politicsQuery = 'INSERT INTO tbl_politics (politicsname, comm_male, comm_female, comm_other, reli_male, reli_female, reli_other, dalit_male, dalit_female, dalit_other, tribes_male, tribes_female, tribes_other, other_male, other_female, other_other) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

  const politicsValues = [politicsname, comm_male, comm_female, comm_other, reli_male, reli_female, reli_other, dalit_male, dalit_female, dalit_other, tribes_male, tribes_female, tribes_other, other_male, other_female, other_other];

  connection.query(politicsQuery, politicsValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Political Party Posted Successful..' })
      console.log("Political Party posted successful..");
    }
  })
})

app.get('/api/politics', (req, res) => {
  const query = "SELECT * FROM tbl_politics";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/politics/:id', (req, res) => {
  const politicsId = req.params.id;
  const { politicsname, comm_male, comm_female, comm_other, reli_male, reli_female, reli_other, dalit_male, dalit_female, dalit_other, tribes_male, tribes_female, tribes_other, other_male, other_female, other_other } = req.body;

  const query = 'UPDATE tbl_politics SET politicsname=?, comm_male=?, comm_female=?, comm_other=?, reli_male=?, reli_female=?, reli_other=?, dalit_male=?, dalit_female=?, dalit_other=?, tribes_male=?, tribes_female=?, tribes_other=?, other_male=?, other_female=?, other_other=? WHERE id=?';
  const values = [politicsname, comm_male, comm_female, comm_other, reli_male, reli_female, reli_other, dalit_male, dalit_female, dalit_other, tribes_male, tribes_female, tribes_other, other_male, other_female, other_other, politicsId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Political Party Updated Successfully' });
      }
    }
  });
});


app.delete('/api/politics/:id', (req, res) => { // Fixed parameter order: (req, res)
  const politicsId = req.params.id;

  const query = "DELETE FROM tbl_politics WHERE id = ?";
  const values = [politicsId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});

// QUERY OF IMPORTANT PLACE

app.post('/api/place', (req, res) => {
  const { placename, ward_id, place_type, place_area, ownership, accessby, hotelno, distance, tourist_internal, tourist_foreign, revenue, annual_prgm_no } = req.body;

  const placeQuery = 'INSERT INTO tbl_place (placename, ward_id, place_type, place_area, ownership, accessby, hotelno, distance, tourist_internal, tourist_foreign, revenue, annual_prgm_no) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';

  const placeValues = [placename, ward_id, place_type, place_area, ownership, accessby, hotelno, distance, tourist_internal, tourist_foreign, revenue, annual_prgm_no];

  connection.query(placeQuery, placeValues, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Place Posted Successful..' })
      console.log("Place posted successful..");
    }
  })
})

app.get('/api/place', (req, res) => {
  const query = "SELECT * FROM tbl_place";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/place/:id', (req, res) => {
  const placeId = req.params.id;
  const { placename, ward_id, place_type, place_area, ownership, accessby, hotelno, distance, tourist_internal, tourist_foreign, revenue, annual_prgm_no } = req.body;

  const query = 'UPDATE tbl_place SET placename=?, ward_id=?, place_type=?, place_area=?, ownership=?, accessby=?, hotelno=?, distance=?, tourist_internal=?, tourist_foreign=?, revenue=?, annual_prgm_no=? WHERE id=?';
  const values = [placename, ward_id, place_type, place_area, ownership, accessby, hotelno, distance, tourist_internal, tourist_foreign, revenue, annual_prgm_no, placeId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'place Updated Successfully' });
      }
    }
  });
});


app.delete('/api/place/:id', (req, res) => { // Fixed parameter order: (req, res)
  const placeId = req.params.id;

  const query = "DELETE FROM tbl_place WHERE id = ?";
  const values = [placeId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});

// QUERY OF EXPORT SOURCES

app.post('/api/export', (req, res) => {
  const { exportward_id, export_res_type, export_material_type, export_material_name, export_qty, export_amnt } = req.body;

  const Query = 'INSERT INTO tbl_export (exportward_id, export_res_type, export_material_type, export_material_name, export_qty, export_amnt) VALUES (?,?,?,?,?,?)';

  const Values = [exportward_id, export_res_type, export_material_type, export_material_name, export_qty, export_amnt];

  connection.query(Query, Values, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Export Posted Successful..' })
      console.log("Export posted successful..");
    }
  })
})

app.get('/api/export', (req, res) => {
  const query = `
    SELECT
      e.id,
      e.exportward_id,
      r.export_resources AS export_res_type,
      m.material_type AS export_material_type,
      e.export_material_name,
      e.export_qty,
      e.export_amnt
    FROM
      tbl_export e
      LEFT JOIN tbl_export_resources r ON e.export_res_type = r.id
      LEFT JOIN tbl_export_material_type m ON e.export_material_type = m.id
  `;

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});




app.put('/api/export/:id', (req, res) => {
  const exportId = req.params.id;
  const { exportward_id, export_res_type, export_material_type, export_material_name, export_qty, export_amnt } = req.body;

  const query = 'UPDATE tbl_export SET exportward_id=?, export_res_type=?, export_material_type=?, export_material_name=?, export_qty=?, export_amnt=? WHERE id=?';
  const values = [exportward_id, export_res_type, export_material_type, export_material_name, export_qty, export_amnt, exportId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Export Updated Successfully' });
      }
    }
  });
});


app.delete('/api/export/:id', (req, res) => { // Fixed parameter order: (req, res)
  const exportId = req.params.id;

  const query = "DELETE FROM tbl_export WHERE id = ?";
  const values = [exportId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});


// START OF EXPORT_RESOURCES_TYPE
app.post('/api/export-resources', (req, res) => {
  const { export_resources } = req.body;

  const Query = 'INSERT INTO tbl_export_resources (export_resources) VALUES (?)';

  const Values = [export_resources];

  connection.query(Query, Values, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Export Resources Posted Successful..' })
      console.log("Export Resources posted successful..");
    }
  })
});
app.get('/api/export-resources', (req, res) => {
  const query = "SELECT * FROM tbl_export_resources";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});


// START OF EXPORT_MATERIAL_TYPE
app.post('/api/export-material-type', (req, res) => {
  const { material_type, material_ie } = req.body;

  const Query = 'INSERT INTO tbl_export_material_type (material_type, material_ie) VALUES (?,?)';

  const Values = [material_type, material_ie];

  connection.query(Query, Values, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Export Material Type Posted Successful..' })
      console.log("Export Material Type posted successful..");
    }
  })
});
app.get('/api/export-material-type', (req, res) => {
  const query = "SELECT * FROM tbl_export_material_type";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

// QUERY OF WATER SOURCE

app.post('/api/water', (req, res) => {
  const { watername, water_ward, water_res_type, usage_details, area, current_status } = req.body;

  const Query = 'INSERT INTO tbl_water (watername, water_ward, water_res_type, usage_details, area, current_status) VALUES (?,?,?,?,?,?)';

  const Values = [watername, water_ward, water_res_type, usage_details, area, current_status];

  connection.query(Query, Values, (error) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' })
    } else {
      res.status(200).json({ message: 'Water Source Posted Successful..' })
      console.log("Water Source posted successful..");
    }
  })
})

app.get('/api/water', (req, res) => {
  const query = "SELECT * FROM tbl_water";

  connection.query(query, (errors, results) => {
    if (errors) {
      console.error('Error executing MySQL query: ', errors);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      res.json(results);
    }
  });
});

app.put('/api/water/:id', (req, res) => {
  const waterId = req.params.id;
  const { watername, water_ward, water_res_type, usage_details, area, current_status } = req.body;

  const query = 'UPDATE tbl_water SET watername=?, water_ward=?, water_res_type=?, usage_details=?, area=?, current_status=? WHERE id=?';
  const values = [watername, water_ward, water_res_type, usage_details, area, current_status, waterId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error('Error executing MySQL query: ', error);
      res.status(500).json({ message: 'Internal Server Error' });
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'Not Found' });
      } else {
        res.json({ message: 'Water Source Updated Successfully' });
      }
    }
  });
});


app.delete('/api/water/:id', (req, res) => { // Fixed parameter order: (req, res)
  const placeId = req.params.id;

  const query = "DELETE FROM tbl_water WHERE id = ?";
  const values = [placeId];

  connection.query(query, values, (error, results) => {
    if (error) {
      console.error("Error executing MySQL query", error);
      res.status(500).json("INTERNAL SERVER ERROR");
    } else {
      if (results.affectedRows === 0) {
        res.status(404).json({ message: 'NOT FOUND' });
      } else {
        res.json({ message: "Data deleted successful" });
      }
    }
  });
});
















// PORT RUNNING

app.listen(port, (err) => {
  if (err) {
    throw err
  } else
    console.log(`server is running at port: ${port}`);
})
