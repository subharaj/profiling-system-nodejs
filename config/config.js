module.exports = {
  jwtSecret: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjgyNjczMzA0LCJqdGkiOiJhMTIwNDkwNTg0ZTc0Y2Q2OWU0NTYwMGE5MTExMzcxYyIsInVzZXJfaWQiOjEyN30',
  jwtRefreshSecret: 'YRjxLpsjRqL7zYuKstXogqioA_P3Z4fiEuga0NCVRcDSc8cy_9msxg',
  jwtExpiresIn: '15m',
  jwtRefreshExpiresIn: '7d'
};